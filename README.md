Ansible Archlinux Prepare
==========================

A role to install Archlinux from Debian Instance

Tested and Used on ArchLinux but it may work on any Linux

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Be Kind
